        var userNameRegEx = /@(\w*)\b/;
        var hashTagRegEx = /#(\w*)\b/;
        var urlLinkRegEx = /(^|[(\s]|&lt;)((?:(?:https?|ftp):\/\/|mailto:).+?)((?:[:?]|\.+)?(?:\s|$)|&gt;|[)])/;
        function myTwitterLinkify(rawText) {
            var stringArray = rawText.split(' ');
            var stringStack = [];
            $.each(stringArray, function () {
                if (userNameRegEx.test(this)) {
                    var userlink = '<a href="http://twitter.com/' + userNameRegEx.exec(this)[0].toString().replace('@', '') + '">' + this.toString() + '</a>';
                    stringStack.push(userlink);
                }
                if (hashTagRegEx.test(this)) {
                    var userlink = '<a href="http://twitter.com/search?q=' + escape(hashTagRegEx.exec(this)[0].toString()) + '">' + this.toString() + '</a>';
                    stringStack.push(userlink);
                }
                if (urlLinkRegEx.test(this)) {
                    var userlink = '<a href="' + urlLinkRegEx.exec(this)[0].toString() + '">' + this.toString() + '</a>';
                    stringStack.push(userlink);
                }
                if (!userNameRegEx.test(this) && !hashTagRegEx.test(this) && !urlLinkRegEx.test(this)) {
                    stringStack.push(this);
                }
            });
            return stringStack.join(' ');
        }