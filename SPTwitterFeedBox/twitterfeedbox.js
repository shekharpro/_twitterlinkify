var username = 'samajshekhar';
var format = 'json';
var count = '6';
var include_retweets = 't';
var theapiurl = 'http://api.twitter.com/1/statuses/user_timeline.' + format + '?callback=?';
var fillReqSucceded = false;
var sptfbFillTimer = 'undefined';
var loadingelem;


/*
Called when DOM is in ready state
*/
$(document).ready(function () {
	fillTweets();
});

/*
Function to fill tweets in the tweet box
*/
function fillTweets() {
	$('#SPTFBLoading').fadeIn('slow');
	$.getJSON(theapiurl,
			{
				screen_name: username,
				count: count,
				trim_user: 't',
				include_rts: include_retweets
			},
			function (data) {
				$('.SPTFBFeed').remove();
				$.each(data, function () {
					var elem = $('<div/>');
					elem.addClass('SPTFBFeed').html(myTwitterLinkify(this.text));                                                        
					$('#SPTFBBody').append(elem);
				});
				fillReqSucceded = true;
				$('#SPTFBLoading').fadeOut('slow');
			});
	if (sptfbFillTimer != 'undefined' && fillReqSucceded === false) { $('#SPTFBLoading').html('Problem in loading Tweets :(<br/><br/>Please wait while we try again...').css({ 'color': 'red' }); sptfbFillTimer = setTimeout('fillTweets()', 20000); return; }
	sptfbFillTimer = setTimeout('fillTweets()', 30000);
	$($('#SPTFBLoading').html('loading tweets...')).css({ 'color': 'black' });
}

/*
This is the code that binds the known elements in a tweet like hashtags, url, @username in an anchor tag.
NOTE: This code is also hosted seperately at http://cdn.bitbucket.org/shekharpro/twitterlinkify/downloads/twitterlinkify-0.1-min.js
*/
var userNameRegEx = /@(\w*)\b/;
var hashTagRegEx = /#(\w*)\b/;
var urlLinkRegEx = /(^|[(\s]|&lt;)((?:(?:https?|ftp):\/\/|mailto:).+?)((?:[:?]|\.+)?(?:\s|$)|&gt;|[)])/;
function myTwitterLinkify(rawText) {
	var stringArray = rawText.split(' ');
	var stringStack = [];
	$.each(stringArray, function () {
		if (userNameRegEx.test(this)) {
			var userlink = '<a href="http://twitter.com/' + userNameRegEx.exec(this)[0].toString().replace('@', '') + '">' + this.toString() + '</a>';
			stringStack.push(userlink);
		}
		if (hashTagRegEx.test(this)) {
			var userlink = '<a href="http://twitter.com/search?q=' + escape(hashTagRegEx.exec(this)[0].toString()) + '">' + this.toString() + '</a>';
			stringStack.push(userlink);
		}
		if (urlLinkRegEx.test(this)) {
			var userlink = '<a href="' + urlLinkRegEx.exec(this)[0].toString() + '">' + this.toString() + '</a>';
			stringStack.push(userlink);
		}
		if (!userNameRegEx.test(this) && !hashTagRegEx.test(this) && !urlLinkRegEx.test(this)) {
			stringStack.push(this);
		}
	});
	return stringStack.join(' ');
}